<?php

Route::get('/', function () {
    return view('index');
});
Route::get('/circus', function () {
    return view('circus');
});
Route::get('/tabagan', function () {
    return view('tabagan');
});
Route::get('/phpmyadmin', function () {
    return view('phpmyadmin.index');
});
Route::get('/addanswer', function () {
    return view('crud/addanswer');
});
Route::get('/showanswers', function () {
    return view('crud/showanswers');
});
Route::resource('crud', 'CRUDController');
