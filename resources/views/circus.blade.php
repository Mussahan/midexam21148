<!DOCTYPE html>
<html>


<head>
  <meta charset="utf-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

<title> Купоны, Скидки и Акции в Казахстане: все скидочные купоны в одном месте на сайте Chocolife.me</title>
<link rel="shortcut icon" href="img/favicon.png"/>
<link rel="stylesheet" href="css/tabagan.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/circus.css">

</head>



<body>
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

  <div id="navigatorRodnoy">
    <div class = "container">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
              <li class="nav-item">
                <div id="first">
                  <a class="nav-link " href="#"></a>
                	<div id="triangle"></div>
              </div>
              </li>
            <li class="nav-item">
              <div id="second">
              <a class="nav-link " href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="third">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="fourth">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="fifth">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="sixth">
              <a class="nav-link" href="#"></a>
            </div>

            </li>
            <li class="nav-item">
            	<div class="rightside" id="reg">
            		<a class = "nav-link" href="#"></a>
            	</div>
            </li>
            <li class="nav-item">
            	<div id="enter">
            		<a class = "nav-link" href="#">Вход</a>
            	</div>
            </li>
            <li class="nav-item">
            	<div id="imgshop">
            		<a class = "nav-link" href="#" id="shop">
            			<div id="balance"><h4>0</h4></div>
            		</a>
            	</div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <div class="inform">
    <div class="container">
      <div class = "row">
        <div class="col-md-3">
          <a href="#" >
            <div id="almaty" class="imageshelp">

            </div>
          </a>
        </div >
        <div class="col-md-3">
          <a href="#" >
            <div id="needhelp" class="imageshelp">

            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" >
            <div id="defence" class="imageshelp" >

            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" >
            <div id="sends" class="imageshelp">

            </div>
          </a>
        </div>
      </div>
    </div>
  </div>

    <div class="searchactions">
    <div class="container">
      <div class = "row">
        <div class="col-md-3">
          <a href="/"><div id="choco" class="imagesactions">

          </div></a>
        </div >
        <div class="col-md-3">
          <div id="nadpis" class="imagesactions">
            <b><p>Главное, чтобы Вы</p>
            <p>были счастливы!</p></b>
          </div>
        </div>
        <div class="col-md-3">
          <div id="searchactionslala" class="imagesactions">
            <input type="search" class="form-control" id="usr" placeholder="Найти среди 622 акций">
          </div>
        </div>
        <div class="col-md-3">
          <a href="#"><div id="searchicon" class="imagesactions">

          </div>
        </a>
        </div>
      </div>
    </div>
  </div>

  <div id="doubleline">
    <div id="categoriesmaker">
      <ul class ="list-inline">
        <li class ="list-inline-item" id="categories">
          <a href="#" > Все </a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Новые </a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a id="red" href="#">Хиты продаж</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a   href="#">Развлечения и Отдых</a>
        </li>
        <li class ="list-inline-item" id="categories" >
          <a href="#">Красота и здоровье</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a href="#" >Спорт</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Товары</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a href="#">Услуги</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Еда</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Туризм,Отели</a>
        </li>
      </ul>
    </div>
  </div>

<div class="Mainpart">
  <div class="offer">
    <div class="container">
    <div id="offer-title">
      <span class="offer-title-begin">Можно купить с 25 января по 31 января</span>
      <span class="offer-title-begin rightstorona"> Можно воспользоваться до 4 февраля 2018 года</span>
    </div>
    <div id="offer-info">
      <h5>Цирк! Цирк! Цирк! Артисты «Большого Московского цирка» и «Цирка Никулина» с новой зажигательной программой ждут вас 4 февраля! Билеты со скидкой</h5>
      <div class="image">
        <img src='img/circus1.jpg' id="circus1">
      </div>
        <div class="coasts">
        <div class="offer-price_main">
          <p>От 1400 тг</p>
        </div>
        <div class="offer-price_old">
          <p>экономия <strong>от 600 тг.</strong></p>
        </div>
        <a href="#"><div class="buy">
          Провести Снова
        </div></a>
        <div class="counter">
          <p>Купили <strong>278 человек.</strong></p>
        </div>
      <div class="time">
        <div id="realtime">
          <p> <img src="img/cloack.png"> Акция завершена</p>
      </div>
      </div>
      <div class="email">
        <img src='img/email.png'>

      </div>
      </div>
    </div>
    </div>
  </div>

    <div class="offer">
    <div class="container">
    <div id="offer-title">
      <span class="offer-title-begin"></span>
      <span class="offer-title-begin rightstorona"></span>
    </div>
    <div id="offer-info">
      <h5></h5>
      <div class="image">
      </div>
        <div class="coasts">
        <div class="offer-price_main">
          <p></p>
        </div>
        <div class="offer-price_old">
          <p><strong></strong></p>
        </div>
        <div class="counter">
          <p><strong></strong></p>
        </div>
      <div class="time">
        <div id="realtime">
          <p></p>
      </div>
      </div>
      <div class="email">

      </div>
      </div>
    </div>
    </div>
  </div>



  <div class="Information">
    <div class ="container">
      <div id="topinfo">

        <div class="offer-meny">
          <ul type ="none" class ="list-inline">
            <li class ="list-inline-item"><div id="infasotka"> <a href="#">Информация </a></div></li>
            <li class ="list-inline-item"><a href="#"> Отзывы(12) </a></li>
            <li class ="list-inline-item"> <a href="#">Вопросы(4)</a></li>
            <li class ="list-inline-item"><a href="#"> Получить 5000 тенге</a></li>
            <li class ="list-inline-item"> <div id="infagolubayasotka"><a href="#" font-color="#30a2e9">Как воспользоваться акцией</a></div></li>
          </ul>
        </div>

    </div>

    <div id="bottominfo">
      <div class="bottominfoleft">
        <img src='img/rash.png' id="rash">
      <p class="condtext"><strong>Условия:</strong></p>

        <ul>

        <li class="nazvanie">
            Сертификат предоставляет возможность посетить Казахский Государственный Цирк.

        </li>
        <li class="nazvanie">
            Выбор мест не предоставляется.</a>
        </li>
        <li class="nazvanie">
            Билеты выдаются в порядке очередности.
        </li>
        <li class="nazvanie">
          <strong>Дата и время проведения: 4 февраля, в 16:00.</strong>
        </li>
        <li class="nazvanie">
          <strong>Адрес проведения:</strong> г. Алматы, пр. Абая, 50, «Казахский Государственный Цирк».
          <br><strong>Сертификат необходимо обменять на билеты в офисе Сhocolife.me до 3 февраля (включительно)</strong>по адресу: г. Алматы, ул. Байзакова, 280, БЦ Almaty Towers, офис 222.</p>
        </li>
        <li class="nazvanie">
            Скидка не суммируется с другими действующими предложениями цирка.

        </li>
        <li class="nazvanie">
            Дети до 4 лет могут посещать цирк бесплатно (без предоставления места).
        </li>
        <li class="nazvanie">
            Ограничений по возрасту нет.

            <p>Согласно внутренним правилам цирка, приносить с собой еду не разрешается.</p>
        </li>
        <li class="nazvanie">
           <strong>Справки по телефону:</strong>
            <p>+7 (727) 346-85-88 (офис Chocolife.me).</p>
        </li>
        <li class="nazvanie">
            Вы можете приобрести неограниченное количество сертификатов по данной акции как для себя, так и в подарок.

        </li>
        <li class="nazvanie">
            <b>Сертификат распечатывать необязательно, достаточно сообщить его номер и SC-код.</b>
        </li>
        <li class="nazvanie">
            Сертификат действителен до 28 февраля 2018 г. (включительно).
        </li>
        <li class="nazvanie">
          <a href="#" class="blue">
             Политика по возврату средств
          </a>
        </li>
        <li class="nazvanie">
          <a href="#" class="blue">
              Стандартные условия каждой акции
           </a>
        </li>
      </ul>
      <p class="condtext"><strong>Адрес:</strong></p>
      <ul>
          <li class="nazvanie">г. Алматы, ул. Байзакова, 280 (уг. ул. Сатпаева), БЦ Алматы Тауэрс, 2 этаж, кабинет 222 (офис Chocolife.me)
          <a href="#" class="blue">
             Посмотреть на карте
          </a>
        </li>
        <li class="nazvanie">
          <p>Телефон :</p>
          <p>+7 (727) 346-85-88 </p>
        </li>
        <li class="nazvanie">
          <p>График работы:</p>
          <p>Ежедневно: c 09:00 до 22:00</p>
        </li>
      </ul>
      </div>

       <div class="bottominforight">
          <img src='img/2giscirc.png' id="rash">


          <div id="video">
            <iframe width="450" height="300" src="https://www.youtube.com/embed/p8W8WsCm-gU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <p class="condtext"><strong>Особенности:</strong></p>
            <li class="nazvanie">
              Артисты «Большого Московского цирка» и «Цирка Никулина» лауреаты и призеры различных мировых фестивалей циркового искусство представляют:

            </li>
            <ul>
            <li class="nazvanie">
              акробаты на подкидной доске;


            </li>

              <li class="nazvanie">
              воздушные гимнасты;


              </li>
              <li class="nazvanie">
              лапундеры (обезьяны);
              </li>
              <li class="nazvanie">
              нубийские козлики;

              </li>
              <li class="nazvanie">
                мини лошадка;
              </li>
            <li class="nazvanie">
              конное трио (высшая школа верховой езды);


            </li>
            <li class="nazvanie">
              собаки;
;


            </li>
            <li class="nazvanie">
              и конечно же клоуны.



            </li>
          </ul>
            </ul>
        <li class="nazvanie">
        Длительность — 2,5 часа.


        </li>

        <li class="nazvanie">
        Артисты:

        </li>
        <ul>
        <li class="nazvanie">
          Ковгар Андрей;


        </li>
        <li class="nazvanie">
          Кох-Кукис Мария;


        </li>
        <li class="nazvanie">
          Гукаев Константин.

        </li>
          </ul>
       </div>

    </div>

  </div>
  <div id="bill">
    <p>Способы оплаты : <img src="img/qiwi.png"> </p>

  </div>
  </div>
</div>

</body>

<footer>
  <div class="footerR">
    <div class="container">
      <div class = "row">
        <div class="col-md-3">
          <h5>Компания</h5>
        </div>
        <div class="col-md-3">
          <h5>Клиентам</h5>
        </div>
        <div class="col-md-3">
          <h5>Партнерам</h5>
        </div>
        <div class="col-md-3">
          <h5>Наше приложение</h5>
        </div>
        <div class="col-md-3">
          O chocolife
        </div>
        <div class="col-md-3">
          Обратная связь
        </div>
        <div class="col-md-3">
          Для нашего бизнеса
        </div>
        <div class="col-md-3">
          Chocolife.me теперь еще удобнее
        </div>
        <div class="col-md-3">
          Пресса о нас
        </div>
        <div class="col-md-3">
          Обучающий видеоролик
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">
          Контакты
        </div>
        <div class="col-md-3">
                    Вопросы и ответы
        </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">
          Публичная оферта
        </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-3">
        <a href="#" >
          <img src='img/appstore-footer.png' id="google">
        </a>
        </div>
        <div class="col-md-3" id="datesdvig"> 
          Chocolife.me|2011-2018
        </div>
        <div class="col-md-3" id="mapsdvig">
          Карта Сайта
        </div>
        <div class="col-md-3" id="chocosdvig">
          Chocolife.me в социальных сетях:
        </div>

          <div class="col-md-3" id="appsdvig">
             <a href="#">
                <img src='img/instawminsta.png'>
              </a>
          </div>

      </div>
    </div>
  </footer>




</html>
