<!DOCTYPE html>
<html>


<head>
  <meta charset="utf-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

<title> Катание на сноуборде одном месте на сайте Chocolife.me</title>
<link rel="shortcut icon" href="img/favicon.png"/>
<link rel="stylesheet" href="css/tabagan.css">
<link rel="stylesheet" href="css/style.css">

</head>



<body>
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

  <div id="navigatorRodnoy">
    <div class = "container">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
              <li class="nav-item">
                <div id="first">
                  <a class="nav-link " href="#"></a>
                	<div id="triangle"></div>
              </div>
              </li>
            <li class="nav-item">
              <div id="second">
              <a class="nav-link " href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="third">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="fourth">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="fifth">
              <a class="nav-link" href="#"></a>
            </div>
            </li>
            <li class="nav-item">
                <div id="sixth">
              <a class="nav-link" href="#"></a>
            </div>

            </li>
            <li class="nav-item">
            	<div class="rightside" id="reg">
            		<a class = "nav-link" href="#"></a>
            	</div>
            </li>
            <li class="nav-item">
            	<div id="enter">
            		<a class = "nav-link" href="#">Вход</a>
            	</div>
            </li>
            <li class="nav-item">
            	<div id="imgshop">
            		<a class = "nav-link" href="#" id="shop">
            			<div id="balance"><h4>0</h4></div>
            		</a>
            	</div>
            </li>          
          </ul>
        </div>
      </nav>
    </div>
  </div>


  <div class="inform">
    <div class="container"> 
      <div class = "row"> 
        <div class="col-md-3"> 
          <a href="#" >
            <div id="almaty" class="imageshelp"> 
              
            </div>
          </a>
        </div > 
        <div class="col-md-3">
          <a href="#" >
            <div id="needhelp" class="imageshelp"> 
              
            </div> 
          </a>
        </div> 
        <div class="col-md-3"> 
          <a href="#" >
            <div id="defence" class="imageshelp" > 
              
            </div>
          </a> 
        </div>
        <div class="col-md-3"> 
          <a href="#" >
            <div id="sends" class="imageshelp"> 
              
            </div>
          </a>
        </div>  
      </div> 
    </div> 
  </div>

    <div class="searchactions"> 
    <div class="container"> 
      <div class = "row"> 
        <div class="col-md-3"> 
          <a href="/"><div id="choco" class="imagesactions"> 
            
          </div></a>
        </div > 
        <div class="col-md-3"> 
          <div id="nadpis" class="imagesactions"> 
            <b><p>Главное, чтобы Вы</p>
            <p>были счастливы!</p></b>
          </div>
        </div> 
        <div class="col-md-3"> 
          <div id="searchactionslala" class="imagesactions"> 
            <input type="search" class="form-control" id="usr" placeholder="Найти среди 622 акций">
          </div>
        </div>
        <div class="col-md-3"> 
          <a href="#"><div id="searchicon" class="imagesactions"> 
         
          </div>
        </a>
        </div>
      </div> 
    </div> 
  </div>

  <div id="doubleline">
    <div id="categoriesmaker">
      <ul class ="list-inline">
        <li class ="list-inline-item" id="categories">
          <a href="#" > Все </a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Новые </a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a id="red" href="#">Хиты продаж</a>  
        </li>
        <li class ="list-inline-item" id="categories">
          <a   href="#">Развлечения и Отдых</a>
        </li>
        <li class ="list-inline-item" id="categories" >
          <a href="#">Красота и здоровье</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a href="#" >Спорт</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Товары</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a href="#">Услуги</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Еда</a>
        </li>
        <li class ="list-inline-item" id="categories">
          <a  href="#">Туризм,Отели</a>
        </li>
      </ul>
    </div>
  </div>

<div class="Mainpart">
  <div class="offer">
    <div class="container">
    <div id="offer-title">
      <span class="offer-title-begin">Можно купить с 22 декабря по 4 февраля</span>
      <span class="offer-title-begin rightstorona"> Можно воспользоваться до 28 февраля 2018 года</span>
    </div>
    <div id="offer-info">
      <h5> Экстремально круто! Абсолютно новый снежный склон ждет вас! Катания на лыжах и сноуборде для взрослых и детей со скидкой до 30% в СРК Табаган!</h5>
      <div class="image">
        <img src='img/tabagan.png' id="tabagan">
      </div>
        <div class="coasts">
        <div class="offer-price_main">
          <p>От 1400 тг</p>
        </div>
        <div class="offer-price_old">
          <p>экономия <strong>от 600 тг.</strong></p>
        </div>
        <a href="#"><div class="buy">
          Купить
        </div></a>
        <div class="counter">
          <p>Купили <strong>2023 человека.</strong></p>
        </div>
      <div class="time">
        <p> До завершения акции осталось:</p>
        <div id="realtime">
          <p> <img src="img/cloack.png"> <strong> 20:02:51</strong></p>
      </div>
      </div>
      <div class="email">
        <img src='img/email.png'>

      </div>
      </div>
    </div>
    </div>
  </div>

  <div class="Information">
    <div class ="container">
      <div id="topinfo">

        <div class="offer-meny">
          <ul type ="none" class ="list-inline">
            <li class ="list-inline-item"><div id="infasotka"> <a href="#">Информация </a></div></li>
            <li class ="list-inline-item"><a href="#"> Отзывы(39) </a></li>
            <li class ="list-inline-item"> <a href="#">Вопросы(153)</a></li>
            <li class ="list-inline-item"><a href="#"> Получить 5000 тенге</a></li>
            <li class ="list-inline-item"> <div id="infagolubayasotka"><a href="#" font-color="#30a2e9">Как воспользоваться акцией</a></div></li>
          </ul>
        </div>
       
    </div>

    <div id="bottominfo">
      <div class="bottominfoleft">
        <img src='img/rash.png' id="rash">
      <p class="condtext"><strong>Условия:</strong></p>
      <ul>
        <li class="nazvanie">
        Сертификат предоставляет возможность посетить спортивно-развлекательный комплекс Табаган.
      </li>
      <p class="typessert">Виды Сертификатов: </p>
        <ul class="realtypesofsert" type="none"> 
          <li class="nazvanie">Дневной абонемент на взрослого (в будни) — 3 000 тг. вместо 4 000 тг.
            <p ><a href="#" class="blue">купить</a></p>

          </li>
          <li class="nazvanie">Дневной абонемент детский (в выходные) — 2 100 тг. вместо 3 000 тг.
            <p ><a href="#" class="blue">купить</a></p>

          </li>

          <li class="nazvanie">Дневной абонемент детский (в будни) — 1 500 тг. вместо 2 000 тг.
            <p ><a href="#" class="blue">купить</a></p>

          </li>
          <li class="nazvanie">Ночной абонемент на взрослого (в выходные) — 2 300 тг. вместо 3 000 тг.

            <p ><a href="#" class="blue">купить</a></p>

          </li>
          <li class="nazvanie">Ночной абонемент на взрослого (в будни) — 1 400 тг. вместо 2 000 тг.
            <p ><a href="#" class="blue">купить</a></p>

          </li>
        </ul>

        <li class="nazvanie"> 
            Купленный сертификат действует только на один склон - на новый или на старый!
        </li>
        <li class="nazvanie">
            После приобретения сертификата дополнительно оплачивается аренда инвентаря. С ценами можете ознакомиться в <a href="#" class="blue">прайс-листе.</a>
        </li>
        <li class="nazvanie">
            Детям с 11 лет необходимо приобретать взрослые абонементы.
        </li>
        <li class="nazvanie">
            Перед тем, как получить услугу, обязательно сообщайте нашему партнеру о том, что Вы обратились по акции Chocolife.me.
        </li>
        <li class="nazvanie">
            <b>Справки по телефонам:</b>
          <p>+7 (775) 666-64-01 (звонки принимаются с 9 до 18:00 в будни),</p>
          <p>+7 (700) 978-54-91,</p>
          <p>+7 (707) 712-01-07.</p>
        </li>
        <li class="nazvanie">
            Вы можете приобрести неограниченное количество сертификатов по данной акции как для себя, так и в подарок.
        </li>
        <li class="nazvanie">
            <b>Сертификат распечатывать необязательно, достаточно сообщить его номер и SC-код.</b>
        </li>
        <li class="nazvanie">
            Сертификат действителен до 28 февраля 2018 г. (включительно).
        </li>
        <li class="nazvanie">
          <a href="#" class="blue">
             Политика по возврату средств
          </a>
        </li>
        <li class="nazvanie">
          <a href="#" class="blue">
              Стандартные условия каждой акции
           </a>
        </li>
      </ul>
      <p class="condtext"><strong>Условия:</strong></p>
      <ul>
          <li class="nazvanie">Алматинская обл., Талгарский район, садоводческое общество, ул. Мичурина, 4, «Бескайнар», база отдыха «Табаган»
          <a href="#" class="blue">
             Посмотреть на карте
          </a>
        </li>
        <li class="nazvanie">
          <p>График работы:</p>
          <p>Ежедневно: c 09:00 до 22:00</p>
        </li>
      </ul>
      </div>

       <div class="bottominforight">
          <img src='img/2gis.png' id="rash">


          <div id="video">
            <iframe width="450" height="300" src="https://www.youtube.com/embed/CIMOwbK8axo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <p class="condtext"><strong>Особенности:</strong></p>
          <ul>
            <li class="nazvanie">
              Спортивно-развлекательный комплекс <b>«Табаган»</b> приглашает вас провести незабываемый зимний отдых с друзьями и близкими.<br>
              Готовы к приключениям? Здесь Вас ждут головокружительные склоны, бодрящий морозный воздух и новые трассы для катания на лыжах и сноуборде. Здесь, у подножья гор Заилийского Алатау, вы также найдете пешие тропы для любителей длительных прогулок, отдельные дорожки для велосипедистов и мотоциклистов, для удобства передвижения и наслаждения шикарными видами – канатную дорогу, уютнейшие финские беседки с пылающим камином и песнями под гитару и многое-многое другое.
            </li>
            <li class="nazvanie">
              Здесь вам рады круглый год, ведь уникальные пейзажи Алматинской области любому вскружат голову:  зимой – белоснежными покровами и склонами, которые так и манят всех любителей активного отдыха; весной – невероятными красками пробуждающейся природы ; летом – яркой зеленью и ароматами разнотравья;  осенью – ароматными яблоками и золотыми листопадами.

            </li>
            <li class="nazvanie">
              Обновленный спортивно-развлекательный комплекс  «Табаган» рад представить новую парковую зону YetiPark. Что же это такое:

            </li>
            <ul>
              <li class="nazvanie">
                загадочная тематика и дизайн, посвященные самому таинственному существу, которое, согласно легенде, обитает среди неприступных заснеженных горных вершин;

              </li>
              <li class="nazvanie">
              3 новых лыжных склона (экстрим трасса, трассы для лыжников и сноубордистов);
              </li>
              <li class="nazvanie">
                5-ти полосная 300 метровая горка: удобная, ровная, скоростная и безопасная, с отдельно выделенными полосами;
              </li>
              <li class="nazvanie">
                футдартс:  игра для самых активных: вам предстоит отточить свои снайперские качества, стараясь пнуть мяч так, чтобы тот попал ближе к центру мишени;
              </li>
            <li class="nazvanie">
              финские беседки, где вы можете согреться и отведать вкуснейшую мраморную говядину  или другие блюда по меню либо приготовить их на гриле самостоятельно, насладиться согревающим глинтвейном.

            </li>
            </ul>
        <li class="nazvanie">
          Номера спортивно-развлекательного комплекса «Табаган» порадуют вас безупречным сервисом Room Service и горячими завтраками. Сами номера оборудованы всем необходимым: горячая и холодная вода, центральное отопление, возможность организовать дополнительные места в номере.

        </li>

        <li class="nazvanie">
          Схема склона
          Протяженность бэби-лифта: 200 м. 
          Протяженность трасс: до 4 км. (экстрим-трасса и трассы для катания на лыжах и сноуборде). 

        </li>
        <li class="nazvanie">
          Сайт партнера: <a href="#" class="blue">www.tabagan.com </a>

        </li>
        <li class="nazvanie">
          Скачать <a href="#" class="blue">прайс-лист услуг   <img src='img/download.png'></a>

        </li>
        <li class="nazvanie">
          Скачать <a href="#" class="blue">схему склона   <img src='img/download.png'></a>
        </li>
          </ul>
          <div class="fortabagan"><string>СРК Табаган в социальных сетях:</string></div>
          <br><img src='img/fortabagan.png' class="fortabagan">
       </div>

    </div>

  </div>
  <div id="bill">
    <p>Способы оплаты : <img src="img/qiwi.png"> <div id="finalbill">Купить</div></p>
    <p><img src="img/tag.png"> Теги: Катание, Катание на лыжах, Катание на санках, Развлекательные комплексы, Санки, Сноуборды, Спортивный комплекс, Табаган, Трасса</p>
  </div>
  </div>
</div>

</body>

<footer>
  <div class="footerR">
    <div class="container">
      <div class = "row"> 
        <div class="col-md-3"> 
          <h5>Компания</h5>
        </div>
        <div class="col-md-3">
          <h5>Клиентам</h5>
        </div>
        <div class="col-md-3"> 
          <h5>Партнерам</h5>
        </div>
        <div class="col-md-3"> 
          <h5>Наше приложение</h5>
        </div> 
        <div class="col-md-3"> 
          O chocolife
        </div>
        <div class="col-md-3">
          Обратная связь
        </div>
        <div class="col-md-3"> 
          Для нашего бизнеса
        </div>
        <div class="col-md-3"> 
          Chocolife.me теперь еще удобнее
        </div> 
        <div class="col-md-3"> 
          Пресса о нас
        </div>
        <div class="col-md-3">
          Обучающий видеоролик
        </div>
        <div class="col-md-3"> 
          
        </div>
        <div class="col-md-3"> 

        </div> 
        <div class="col-md-3"> 
          Контакты
        </div>
        <div class="col-md-3">
                    Вопросы и ответы
        </div>
        <div class="col-md-3"> 
        </div>
        <div class="col-md-3"> 
          
        </div> 
        <div class="col-md-3"> 
          
        </div>
        <div class="col-md-3">
          Публичная оферта
        </div>
        <div class="col-md-3"> 
        </div>
        <div class="col-md-3"> 
        <a href="#" >
          <img src='img/appstore-footer.png' id="google">
        </a>
        </div> 
        <div class="col-md-3" id="datesdvig"> 
          Chocolife.me|2011-2018
        </div>
        <div class="col-md-3" id="mapsdvig">
          Карта Сайта
        </div>
        <div class="col-md-3" id="chocosdvig"> 
          Chocolife.me в социальных сетях:
        </div>
   
          <div class="col-md-3" id="appsdvig"> 
             <a href="#">
                <img src='img/instawminsta.png'>
              </a>  
          </div> 
      
      </div> 
    </div>
  </footer>




</html>
