<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

</head>

@extends('master')
@section('content')
<div class="container">
  <form method="post" action="{{url('crud')}}">
    <div class="form-group row">
      {{csrf_field()}}
      <div class="col-sm-5">
        <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="@mail" name="title">
      </div>
    </div>
    <div class="form-group row">
      <label for="smFormGroupInput"></label>
      <div class="col-sm-5">
        <textarea name="post" rows="8" cols="80" width="950px" placeholder="Answers"></textarea>
      </div>
    </div>
      <input type="submit">
  </form>
<p><a href="/crud"> <button type="button" class="btn btn-success">Ask question</button></a></p>
</div>
@endsection
